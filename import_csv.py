import csv
import os

def importer_donnees_csv(chemin_fichier_csv):
    # Définir les listes pour stocker les données
    temps = []
    coord_x = []
    coord_y = []

    # Lire le fichier CSV
    with open(chemin_fichier_csv, 'r') as fichier_csv:
        lecteur_csv = csv.reader(fichier_csv)

        # Ignorer l'en-tête s'il y en a un
        next(lecteur_csv, None)

        # Parcourir les lignes du fichier CSV
        for ligne in lecteur_csv:
            # Ajouter les données aux listes respectives
            temps.append(float(ligne[0]))
            coord_x.append(float(ligne[1]))
            coord_y.append(float(ligne[2]))

    # Retourner les données importées
    return temps, coord_x, coord_y


# Chemin vers le fichier CSV
def list_subfiles(repertoire):
    chemins_sous_fichiers = []

    # Parcourir le répertoire
    for dossier_parent, sous_dossiers, fichiers in os.walk(repertoire):
        for fichier in fichiers:
            # Construire le chemin absolu du fichier
            chemin_absolu = os.path.join(dossier_parent, fichier)
            # Ajouter le chemin à la liste
            chemins_sous_fichiers.append(chemin_absolu)

    return chemins_sous_fichiers
