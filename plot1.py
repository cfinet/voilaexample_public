import dearpygui.dearpygui as dpg
import matplotlib.pyplot as plt
from import_csv import importer_donnees_csv
from import_csv import list_subfiles
import numpy as np
import os
import matplotlib.cm as cm
import distinctipy



list_file_dir = list_subfiles('data/circle')
if list_file_dir:
    # Liste pour stocker les données de chaque série
    series_data = []
    # number of colours to generate
    N = len(list_file_dir)
    print("nb N :", N)
    # generate N visually distinct colours
    colors = distinctipy.get_colors(N)
 
    # Boucle pour importer les données de chaque fichier
    for fichier in list_file_dir:
        temps, coord_x, coord_y = importer_donnees_csv(fichier)
        series_data.append((temps, coord_x, coord_y))
        
    #couleurs = cm.get_cmap('ocean', len(series_data))
    # Création du graphique avec une boucle
    for i, (temps, coord_x, coord_y) in enumerate(series_data):
        couleur = colors[i] # Utilisation de couleurs différentes pour chaque série
        #label = f'Points Série {i+1}'
        plt.scatter(coord_x, coord_y, color=couleur, s=5)

    # Ajout de titres et de légendes
    plt.title('Trajectories')
    plt.xlabel('X')
    plt.ylabel('Y')
    #plt.legend()

    # Affichage du graphique
    plt.show()
else:
    print("Aucun sous-fichier trouvé.")